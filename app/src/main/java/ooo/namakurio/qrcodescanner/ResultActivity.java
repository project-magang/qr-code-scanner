package ooo.namakurio.qrcodescanner;

import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ResultActivity extends AppCompatActivity {

    EditText et_result;
    Button btn_copy,btn_bukasitus,btn_share;

    String hasilSitus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        et_result = (EditText)findViewById(R.id.et_result);
        btn_copy = (Button)findViewById(R.id.btn_copy);
        btn_bukasitus = (Button)findViewById(R.id.btn_bukasitus);
        btn_share = (Button)findViewById(R.id.btn_share);

        final Bundle bundle = getIntent().getExtras();
        et_result.setText(bundle.getString("ResultScan"));

        btn_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clipData = ClipData.newPlainText("Result",et_result.getText().toString());
                clipboard.setPrimaryClip(clipData);
                Toast.makeText(ResultActivity.this,"Berhasil dicopy ke clipboard",Toast.LENGTH_SHORT).show();
            }
        });
        btn_bukasitus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!bundle.getString("ResultScan").startsWith("http://") && !bundle.getString("ResultScan").startsWith("https://")){
                    hasilSitus = "https://www.google.com/search?q="+et_result.getText().toString()+"&oq="+et_result.getText().toString();
                }else{
                    hasilSitus = et_result.getText().toString();
                }
                try {
                    Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(hasilSitus));
                    startActivity(myIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(ResultActivity.this, "Tidak ada browser yang terinstal di Hp Anda.",  Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, et_result.getText().toString());
                intent.setType("text/plain");
                startActivity(Intent.createChooser(intent,"Share dengan"));
            }
        });
    }
}
